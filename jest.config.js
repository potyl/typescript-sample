module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    clearMocks: true,
    testMatch: ['**/tests/**/*.{js,ts}'],
};
